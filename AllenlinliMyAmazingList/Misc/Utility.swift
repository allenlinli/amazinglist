//
//  Utility.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import Foundation
import UIKit

protocol CellReuseIdentifier {
    static var reuseIdentifier: String { get }
}

extension CellReuseIdentifier {
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
}

extension UICollectionViewCell: CellReuseIdentifier {}

extension UITableViewCell: CellReuseIdentifier{}




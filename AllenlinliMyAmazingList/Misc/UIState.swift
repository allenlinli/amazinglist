//
//  UIState.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2020/1/6.
//  Copyright © 2020 ikala. All rights reserved.
//

import Foundation

enum UIState: Equatable {
    enum LoadingState: Equatable {
        case reload
        case loadMore
    }

    enum ContentState: Equatable {
        case partial
        case complete
        case empty
    }

    case initial
    case loading(LoadingState)
    case content(ContentState)
    case error
    case apiError
}

extension UIState {
    func isLoading() -> Bool {
        switch self {
        case (.loading(_)):
            return true
        default:
            return false
        }
    }
}

protocol UIStateDelegateWithOutState: class {
    func stateDidChanged()
}

protocol UIStateDelegate: class {
    func stateDidChanged(state: UIState)
}


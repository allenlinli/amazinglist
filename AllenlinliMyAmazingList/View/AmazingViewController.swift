//
//  ViewController.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/14.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit
import WebKit
import KKBOXOpenAPISwift

class AmazingViewController: UIViewController {

    var testData: [String]?
    @objc private var viewModel: AmazingViewControllerViewModel!
    var tableViewModelObservation: NSKeyValueObservation?
    var collectionViewModelObservation: NSKeyValueObservation?

    @IBOutlet weak var featuredPlaylistsTableView: FeaturedPlaylistsTableView!
    @IBOutlet weak var ChartsCollectionView: ChartsCollectionView!
    var spinner = UIActivityIndicatorView(style: .large)

    func configure(viewModel: AmazingViewControllerViewModel) {
        self.viewModel = viewModel
        viewModel.uiDelegate = self

        //updateTableViewModel
        tableViewModelObservation = observe(
            \.viewModel?.tableViewModel,
            options: [.new]
        ) { [weak self] object, change in
            if let tableViewModel = change.newValue as? FeaturedPlaylistsTableViewModel,
                let strongSelf = self {
                strongSelf.featuredPlaylistsTableView.configure(viewModel:  tableViewModel)
            }
        }

        //updateCollectionView
        collectionViewModelObservation = observe(
            \.viewModel?.collectionViewModel,
            options: [.new]
        ) { [weak self] object, change in
            if let collectionViewModel = change.newValue as? ChartsCollectionViewModel,
                let strongSelf = self {
                strongSelf.ChartsCollectionView.configure(viewModel:  collectionViewModel)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //setup spinner
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        //setup viewModel
        let API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        viewModel = AmazingViewControllerViewModel(API: API)
        configure(viewModel: viewModel)

        //login
        viewModel.fetchCredential(callback: { (_) in
            print("did fetchCredential")
        })
    }

    @IBSegueAction func showWebViewController(_ coder: NSCoder) -> UIViewController? {
        return WebViewController(coder: coder)
    }
}

extension AmazingViewController: UIStateDelegateWithOutState {
    func stateDidChanged() {
        switch viewModel.state {
        case .loggedIn(_):
            print("did logged in")
            spinner.stopAnimating()
        case .notLoggedIn(let error):
            print(error!.localizedDescription)
            spinner.stopAnimating()
        case .logging:
            print("logging...")
            spinner.startAnimating()
        }
    }
}

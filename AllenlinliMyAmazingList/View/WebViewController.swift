//
//  WebViewController.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/14.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let urlString = "https://www.apple.com"
        guard let url = URL(string:urlString) else {
            return
        }
        webView.load(URLRequest(url: url))
        webView.navigationDelegate = self
    }
}

extension WebViewController: WKNavigationDelegate {
    
}

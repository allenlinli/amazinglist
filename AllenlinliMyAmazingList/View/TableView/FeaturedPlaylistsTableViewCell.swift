//
//  FeaturedPlaylistsTableViewCell.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/14.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit
import KKBOXOpenAPISwift
import Kingfisher

class FeaturedPlaylistsTableViewCell: UITableViewCell {
    var imageDownloadTask: DownloadTask?
    var viewModel: FeaturedPlaylistsTableViewCellViewModel? {
        didSet {
            titleLabel.text = viewModel?.title
            bodyLabel.text = viewModel?.body
            let processor = DownsamplingImageProcessor(size: playListImageView.frame.size)
                |> RoundCornerImageProcessor(cornerRadius: 20)
            playListImageView.kf.indicatorType = .activity
            imageDownloadTask = playListImageView.kf.setImage(
                with: viewModel?.imageURL,
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(_):
//                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    break
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playListImageView: UIImageView!
    @IBOutlet weak var bodyLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        imageDownloadTask?.cancel()
    }

    func configure(viewModel: FeaturedPlaylistsTableViewCellViewModel) {
        self.viewModel = viewModel
    }
}

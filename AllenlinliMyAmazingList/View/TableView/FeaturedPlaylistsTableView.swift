//
//  FeaturedPlaylistsTableView.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/14.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit

class FeaturedPlaylistsTableView: UITableView {
    func configure(viewModel: FeaturedPlaylistsTableViewModel) {
        self.viewModel = viewModel
        viewModel.uiDelegate = self
        self.delegate = self
        self.dataSource = self
    }

    private(set) var viewModel: FeaturedPlaylistsTableViewModel?
}

extension FeaturedPlaylistsTableView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.playlistsCount ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeaturedPlaylistsTableViewCell.reuseIdentifier, for: indexPath) as? FeaturedPlaylistsTableViewCell,
            let cellViewModel = viewModel?.getCellViewModel(at: indexPath) else {
                return FeaturedPlaylistsTableViewCell()
        }
        cell.configure(viewModel: cellViewModel)
        return cell
    }
}

extension FeaturedPlaylistsTableView: UITableViewDelegate {

}

extension FeaturedPlaylistsTableView: UIStateDelegateWithOutState {
    func stateDidChanged() {
        reloadData()
    }
}

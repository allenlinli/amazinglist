//
//  ChartsCollectionViewCell.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit
import KKBOXOpenAPISwift
import Kingfisher

class ChartsCollectionViewCell: UICollectionViewCell {
    var imageDownloadTask: DownloadTask?
    var viewModel: ChartsCollectionViewCellViewModel? {
        didSet {
            let processor = DownsamplingImageProcessor(size: moodStationImageView.frame.size)
                |> RoundCornerImageProcessor(cornerRadius: 20)
            moodStationImageView.kf.indicatorType = .activity
            imageDownloadTask = moodStationImageView.kf.setImage(
                with: viewModel?.imageURL,
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(_):
    //                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    break
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        moodStationImageView?.layer.cornerRadius = (moodStationImageView?.frame.width ?? 0) / 2.0
        moodStationImageView.layer.shouldRasterize = true
    }

    @IBOutlet weak var moodStationImageView: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        imageDownloadTask?.cancel()
    }

    func configure(viewModel: ChartsCollectionViewCellViewModel) {
        self.viewModel = viewModel
    }
}

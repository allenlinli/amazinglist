//
//  ChartsCollectionCellView.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/14.
//  Copyright © 2019 ikala. All rights reserved.
//

import UIKit

class ChartsCollectionView: UICollectionView {
    func configure(viewModel: ChartsCollectionViewModel) {
        self.viewModel = viewModel
        viewModel.uiDelegate = self
        self.delegate = self
        self.dataSource = self
    }

    private(set) var viewModel: ChartsCollectionViewModel?
}

extension ChartsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.ChartsCount ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartsCollectionViewCell.reuseIdentifier, for: indexPath) as? ChartsCollectionViewCell,
            let cellViewModel = viewModel?.getCellViewModel(at: indexPath) else {
                return ChartsCollectionViewCell()
        }
        cell.configure(viewModel: cellViewModel)
        return cell
    }
}

extension ChartsCollectionView: UICollectionViewDelegate {

}

extension ChartsCollectionView: UIStateDelegate {
    func stateDidChanged(state: UIState) {
        reloadData()
    }
}

extension ChartsCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60.0, height: 60.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 105.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50.0
    }
}

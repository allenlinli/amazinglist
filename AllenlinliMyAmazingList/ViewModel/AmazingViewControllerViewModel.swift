//
//  AmazingViewControllerViewModel.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import Foundation
import KKBOXOpenAPISwift

let FetchInitialChartsDuration = 60.0

class AmazingViewControllerViewModel: NSObject {
    private(set) var errorDescription: String?
    private(set) var state: UIState = .notLoggedIn(nil) {
        didSet {
            switch state {
            case .logging:
                break
            case .loggedIn(_):
                tableViewModel = FeaturedPlaylistsTableViewModel(API: self.API)
                tableViewModel?.fetchFeaturedPlaylists(callback: { (_) in
                    print("did fetchFeaturedPlaylists")
                })

                collectionViewModel = ChartsCollectionViewModel(API: self.API)

                timer?.invalidate()
                timer = Timer.scheduledTimer(withTimeInterval: FetchInitialChartsDuration, repeats: true) { [weak self] (timer) in
                    self?.collectionViewModel?.fetchInitialCharts(callback: { (_) in
                        print("did fetchInitialCharts")
                    })
                }
                timer?.fire()
                break
            case .notLoggedIn(let error):
                errorDescription = error?.localizedDescription
                timer?.invalidate()
                timer = nil
                break
            }
            uiDelegate?.stateDidChanged()
        }
    }

    private var API: KKBOXOpenAPI
    @objc dynamic private(set) var tableViewModel: FeaturedPlaylistsTableViewModel?
    @objc dynamic private(set) var collectionViewModel: ChartsCollectionViewModel?
    weak var uiDelegate: UIStateDelegateWithOutState?
    private var timer: Timer?

    enum UIState {
        case loggedIn(KKAccessToken)
        case logging
        case notLoggedIn(Error?)
    }
    
    init(API: KKBOXOpenAPI) {
        self.API = API
        super.init()
    }

    func fetchCredential(callback: @escaping (_ state: UIState) -> ()) {
        do {
            state = .logging
            try self.API.fetchAccessTokenByClientCredential { [weak self] (result) in
                switch result {
                case .success(let accessToken):
                    self?.state = .loggedIn(accessToken)
                    break
                case .error(let error):
                    self?.state = .notLoggedIn(error)
                    break
                }
                if let strongSelf = self {
                    callback(strongSelf.state)
                }
            }.resume()

        } catch let error {
            state = .notLoggedIn(error)
        }
    }
}

//
//  FeaturedPlaylistsTableViewModel.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import Foundation
import KKBOXOpenAPISwift

class FeaturedPlaylistsTableViewModel: NSObject {
    let FetchFeaturedPlaylistsLimit = 50

    weak var uiDelegate: UIStateDelegateWithOutState?

    private(set) var errorMessage: String?
    var playlistsCount: Int {
        return playlistList?.playlists.count ?? 0
    }
    private var playlistList: KKPlaylistList? {
        didSet {
            featuredPlaylistsTableViewCellViewModels = playlistList?.playlists.map({ (playlistInfo) -> FeaturedPlaylistsTableViewCellViewModel in
                return FeaturedPlaylistsTableViewCellViewModel(playlist: playlistInfo)
            })
        }
    }
    
    private var featuredPlaylistsTableViewCellViewModels: [FeaturedPlaylistsTableViewCellViewModel]?
    private var API: KKBOXOpenAPI

    init(API: KKBOXOpenAPI) {
        self.API = API
        super.init()
    }

    private(set) var state: State = .initial {
        didSet {
            switch state {
            case .loaded(let playlistList):
                self.playlistList = playlistList
                break
            case .error(let error):
                self.errorMessage = error.localizedDescription
                break
            default:
                print("default")
            }

            uiDelegate?.stateDidChanged()
        }
    }

    enum State {
        case initial
        case empty
        case loaded(KKPlaylistList)
        case error(Error)
    }

    func fetchFeaturedPlaylists(callback: @escaping (_ state: State) -> ()) {
        do {
            try self.API.fetchFeaturedPlaylists(limit: FetchFeaturedPlaylistsLimit) { [weak self] (result)  in
                switch result {
                case .success(let playlistList):
                    self?.state = .loaded(playlistList)

                    callback(.loaded(playlistList))
                    break
                case .error(let error):
                    self?.state = .error(error)
                    callback(.error(error))
                    break
                }
            }.resume()
        } catch {
            state = .error(error)
            callback(.error(error))
        }
    }

    func getCellViewModel(at indexPath: IndexPath) -> FeaturedPlaylistsTableViewCellViewModel? {
        return featuredPlaylistsTableViewCellViewModels?[indexPath.row]
    }
}

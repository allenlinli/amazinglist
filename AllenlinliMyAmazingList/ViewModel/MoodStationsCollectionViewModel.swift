//
//  ChartsCollectionViewModel.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2020/1/6.
//  Copyright © 2020 ikala. All rights reserved.
//

import Foundation
import KKBOXOpenAPISwift


class ChartsCollectionViewModel: NSObject {
    let FetchChartsLimit = 20

    weak var uiDelegate: UIStateDelegate?

    private(set) var errorMessage: String?
    private var API: KKBOXOpenAPI
    var ChartsCount: Int {
        return playlistList?.playlists.count ?? 0
    }

    init(API: KKBOXOpenAPI) {
        self.API = API
        super.init()
    }

    private var playlistList: KKPlaylistList? {
        didSet {
            ChartsCollectionCellViewModels = playlistList?.playlists.map ({ (playlist) -> ChartsCollectionViewCellViewModel in
                return ChartsCollectionViewCellViewModel(playlist: playlist)
            })
        }
    }
    private var ChartsCollectionCellViewModels: [ChartsCollectionViewCellViewModel]?

    private(set) var state: UIState = .initial {
        didSet {
            guard oldValue != state else {
                return
            }
            uiDelegate?.stateDidChanged(state: state)
        }
    }

    func fetchInitialCharts(callback: @escaping (_ state: UIState) -> ()) {
        guard !self.state.isLoading() else {
            return
        }

        fetchCharts(offSet: 0, callback: callback)
    }

    func fetchMoreCharts(callback: @escaping (_ state: UIState) -> ()) {
        guard !self.state.isLoading() && self.state != .content(.complete) && self.state != .content(.empty) else {
            return
        }

        fetchCharts(offSet: ChartsCount,callback: callback)
    }

    private func fetchCharts(offSet: Int, callback: @escaping (_ state: UIState) -> ()) {
        if offSet == 0 {
            self.state = .loading(.reload)
        } else {
            self.state = .loading(.loadMore)
        }

        do {
            try self.API.fetchCharts(limit: FetchChartsLimit) { [weak self] (result)  in
                guard let strongSelf = self else { return }

                switch result {
                case .success(let playlistList):
                    guard playlistList.playlists.count > 0 else {
                        strongSelf.state = .content(.empty)
                        callback(.content(.empty))
                        return
                    }
                    strongSelf.state = .content(.partial)
                    strongSelf.playlistList = playlistList
                    callback(.content(.partial))
                    break
                case .error(_):
                    strongSelf.state = .error
                    strongSelf.errorMessage = "API error"
                    callback(.error)
                    break
                }
            }.resume()
        } catch {
            state = .error
            callback(.error)
        }
    }

    func getCellViewModel(at indexPath: IndexPath) -> ChartsCollectionViewCellViewModel? {
        return ChartsCollectionCellViewModels?[indexPath.row]
    }
}


//
//  ChartsCollectionViewCellViewModel.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2020/1/6.
//  Copyright © 2020 ikala. All rights reserved.
//

import Foundation
import KKBOXOpenAPISwift

class ChartsCollectionViewCellViewModel: NSObject {
    private(set) var imageURL: URL?

    private var playlist: KKPlaylistInfo? {
        didSet {
            self.imageURL = playlist?.images.first?.url
        }
    }

    init(playlist: KKPlaylistInfo) {
        self.playlist = playlist
        self.imageURL = playlist.images.first?.url
        super.init()
    }
}

//
//  FeaturedPlaylistsTableViewCellViewModel.swift
//  AllenlinliMyAmazingList
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import Foundation
import KKBOXOpenAPISwift

class FeaturedPlaylistsTableViewCellViewModel: NSObject {
    private(set) var title: String!
    private(set) var body: String?
    private(set) var imageURL: URL?

    private var playlist: KKPlaylistInfo {
        didSet {
            self.title = playlist.title
            self.body = playlist.playlistDescription
            self.imageURL = playlist.images.first?.url
        }
    }

    init(playlist: KKPlaylistInfo) {
        self.playlist = playlist
        self.title = playlist.title
        self.body = playlist.playlistDescription
        self.imageURL = playlist.images.first?.url
        super.init()
    }
}

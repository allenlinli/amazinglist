//
//  ChartsCollectionViewModelWithNetworkTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2020/1/6.
//  Copyright © 2020 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class ChartsCollectionViewModelWithNetworkTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!
    var ChartsCollectionViewModel: ChartsCollectionViewModel!

    override func setUp() {
        super.setUp()
        API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        amazingViewControllerViewModel = AmazingViewControllerViewModel(API: self.API)
        ChartsCollectionViewModel = ChartsCollectionViewModel(API: self.API)
    }

    func testFetchInitialCharts() {
        let expectation = self.expectation(description: "testFetchFeaturedPlaylists")
        self.ChartsCollectionViewModel.fetchInitialCharts {[weak self] (state) in
            guard let strongSelf = self else {
                return
            }

            switch strongSelf.ChartsCollectionViewModel.state {
            case .content(.empty):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount == 0)
            case .content(.partial):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount > 0)
            case .content(.complete):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount > 0)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 5)
    }

    func testFetchMoreCharts() {
        let expectation = self.expectation(description: "testFetchFeaturedPlaylists")
        self.ChartsCollectionViewModel.fetchMoreCharts {[weak self] (state) in
            guard let strongSelf = self else {
                return
            }

            switch strongSelf.ChartsCollectionViewModel.state {
            case .content(.empty):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount == 0)
            case .content(.partial):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount > 0)
            case .content(.complete):
                XCTAssert(strongSelf.ChartsCollectionViewModel.ChartsCount > 0)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 5)
    }
}

//
//  AmazingViewControllerViewModelTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class AmazingViewControllerViewModelTests: XCTestCase {
    var API: KKBOXOpenAPI!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
    }

    func testInit() {
        let amazingViewControllerViewModel = AmazingViewControllerViewModel(API: API)
        switch amazingViewControllerViewModel.state {
            case .notLoggedIn(_):
                XCTAssert(true)
            default:
                XCTFail()
        }
    }

    func testGetFeaturedPlaylistsTableViewModel() {
        _ = AmazingViewControllerViewModel(API: API)
        XCTAssert(true)
    }
}

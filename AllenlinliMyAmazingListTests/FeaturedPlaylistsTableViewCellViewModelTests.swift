//
//  FeaturedPlaylistsTableViewCellViewModelTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2019/12/17.
//  Copyright © 2019 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class FeaturedPlaylistsTableViewCellViewModelTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!
    var featuredPlaylistsTableViewModel: FeaturedPlaylistsTableViewModel!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        self.amazingViewControllerViewModel = AmazingViewControllerViewModel(API: self.API)
        self.featuredPlaylistsTableViewModel = FeaturedPlaylistsTableViewModel(API: self.API)
    }

    func testFeaturedPlaylistsTableViewCellViewModel() {
        let exp = self.expectation(description: "testFeaturedPlaylistsTableViewCellViewModel")
        self.featuredPlaylistsTableViewModel.fetchFeaturedPlaylists { (state) in
            switch state {
            case .loaded(let playlistList):
                let featuredPlaylistsTableViewCellViewModel = FeaturedPlaylistsTableViewCellViewModel(playlist: playlistList.playlists[0])
                XCTAssert(featuredPlaylistsTableViewCellViewModel.title != nil)
                XCTAssert(featuredPlaylistsTableViewCellViewModel.body != nil)
                XCTAssert(featuredPlaylistsTableViewCellViewModel.imageURL != nil)
            default:
                XCTFail()
            }
            exp.fulfill()
        }
        self.wait(for: [exp], timeout: 5)
    }
}


//
//  FeaturedPlaylistsTableViewModelTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2019/12/17.
//  Copyright © 2019 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class FeaturedPlaylistsTableViewModelTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!
    var featuredPlaylistsTableViewModel: FeaturedPlaylistsTableViewModel!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        self.amazingViewControllerViewModel = AmazingViewControllerViewModel(API: self.API)
        self.featuredPlaylistsTableViewModel = FeaturedPlaylistsTableViewModel(API: self.API)
    }

    func testInit() {
        switch featuredPlaylistsTableViewModel.state {
        case .initial:
            XCTAssert(true)
        default:
            XCTFail()
        }
    }
}

//
//  ChartsCollectionViewModelTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2020/1/6.
//  Copyright © 2020 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class ChartsCollectionViewModelTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!
    var ChartsCollectionViewModel: ChartsCollectionViewModel!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        self.amazingViewControllerViewModel = AmazingViewControllerViewModel(API: self.API)
        self.ChartsCollectionViewModel = ChartsCollectionViewModel(API: self.API)
    }

    func testInit() {
        switch ChartsCollectionViewModel.state {
        case .initial:
            XCTAssert(true)
        default:
            XCTFail()
        }
    }
}

//
//  FeaturedPlaylistsTableViewModelWithNetworkTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2019/12/17.
//  Copyright © 2019 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class FeaturedPlaylistsTableViewModelWithNetworkTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!
    var featuredPlaylistsTableViewModel: FeaturedPlaylistsTableViewModel!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        self.amazingViewControllerViewModel = AmazingViewControllerViewModel(API: self.API)
        self.featuredPlaylistsTableViewModel = FeaturedPlaylistsTableViewModel(API: self.API)
    }

    func testFetchFeaturedPlaylists() {
        let exp = self.expectation(description: "testFetchFeaturedPlaylists")
        self.featuredPlaylistsTableViewModel.fetchFeaturedPlaylists { (state) in
            switch self.featuredPlaylistsTableViewModel.state {
            case .loaded(let playlistList):
                XCTAssert(playlistList.playlists.count > 0)
            default:
                XCTFail()
            }
            exp.fulfill()
        }
        self.wait(for: [exp], timeout: 5)
    }

    func testGetCellViewModel() {
        let exp = self.expectation(description: "testGetCellViewModel")
        self.featuredPlaylistsTableViewModel.fetchFeaturedPlaylists { (state) in
            switch self.featuredPlaylistsTableViewModel.state {
            case .loaded(_):
                let indexPath = IndexPath(row: 0, section: 0)
                let cellViewModel = self.featuredPlaylistsTableViewModel.getCellViewModel(at: indexPath)
                XCTAssert(cellViewModel != nil)
            default:
                XCTFail()
            }
            exp.fulfill()
        }
        self.wait(for: [exp], timeout: 5)
    }
}


//
//  AmazingViewControllerViewModelWithNetworkTests.swift
//  AllenlinliMyAmazingListTests
//
//  Created by Allen and Kim on 2019/12/16.
//  Copyright © 2019 ikala. All rights reserved.
//

import XCTest
import KKBOXOpenAPISwift
@testable import AllenlinliMyAmazingList

class AmazingViewControllerViewModelWithNetworkTests: XCTestCase {
    var API: KKBOXOpenAPI!
    var amazingViewControllerViewModel: AmazingViewControllerViewModel!

    override func setUp() {
        super.setUp()
        self.API = KKBOXOpenAPI(clientID: KKBOXAPIConstants.testClientID, secret: KKBOXAPIConstants.testSecret)
        self.amazingViewControllerViewModel = AmazingViewControllerViewModel(API: API)
    }

    func testFetchCredential() {
        let exp = self.expectation(description: "testFetchCredential")
        amazingViewControllerViewModel.fetchCredential(callback: { (state) in
            switch self.amazingViewControllerViewModel.state {
                case .loggedIn(_):
                    XCTAssert(true)
                default:
                    XCTFail()
            }
            exp.fulfill()
        })
        self.wait(for: [exp], timeout: 5)
    }
}
